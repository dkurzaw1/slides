
---

<section data-background="#000000" data-background-video="img/Network.mp4" data-background-video-loop data-background-video-muted  data-background-opacity="0.1">

</br>
</br>

Forschungsdaten
<!-- .element: style="font-size:300%; font-family: Impact; color:Maroon; font-variant: small-caps; text-shadow: 2px 2px grey" -->

Eine kleine Einführung

</br>
</br>
</br>
</br>
</br>


[Daniel Kurzawe](mailto:kurzawe@sub.uni-goettingen.de)

<!-- .element: style="font-size:50%;" -->


</section>



---

<!-- .slide: data-background="img/StockSnap_H07QQ5ZX7S.jpg" data-background-opacity="0.1"-->
## Überblick

--

### Themen
1. Daten und digitale Objekte
2. Forschungsdaten im Forschungsprozess
3. Herausforderungen
4. Nachhaltigkeit
5. Forschungsdatenmanagement
6. Forschungsförderungen

note: 
- Je nach Interesse gerne Schwerpunkte vertiefen
- ... weitere Themen?

--

### Beispiel: Digitale Edition 

<img src="img/fontane.png" width="800"  />

[Edition: Theodor Fontane Notizbücher](https://fontane-nb.dariah.eu/)

note:
- genetisch-kritische und kommentierte Hybrid-Edition von Theodor Fontanes Notizbüchern 
- Entsteht an der Theodor Fontane-Arbeitsstelle der Universität Göttingen und an der Niedersächsischen Staats- und Universitätsbibliothek Göttingen. 
- Neuer Blick auf Notizbücher
- Links Faksimile, Mitte Strukturansicht, Rechts TEI/XML Daten
- Schönes Beispiel: Zeigt die Potentiale aber auch Herausforderungen 
- Erlaubt digitale Analyse; Aber die Infrastrukur und Daten müssen weiterhin gepflegt werden
- Aber bevor wir in die Tiefe gehen: Erstmal die Grundlagen


---

<!-- .slide: data-background="img/StockSnap_TIV258VG3N.jpg" data-background-opacity="0.1"-->
## Digitale Daten und Objekte

--

### Was sind Forschungsdaten?

--

### Begriffsdefinition

- Unterschiedliche Definitionsansätze: Unterscheiden sich nach Perspektive und Definitionsbereich
- Abgrenzung zu allgemeinen Daten ist schwierig, da die Forschung den Anspruch erhebt, alles unteruchen zu können 

--

### Arbeitsdefinition nach 
#### [forschungsdaten.org](https://www.forschungsdaten.org/index.php/Forschungsdaten#cite_note-.E2.80.9Ckindling.E2.80.9C-1)
> Forschungsdaten bezeichnen (digitale) Daten, die je nach
> Fachkontext Gegenstand eines Forschungsprozesses sind,
> während eines Forschungsprozesses entstehen oder sein Ergebnis sind.

--

### Beispiel: Forschungsdaten in den Geisteswissenschaften
- Annotationen (in Texten),
- Quellen und Quellenanalysen,
- Empirische Erhebungen,
- Fotografien, Scans, Bild- und Tonaufnahmen,
- Visualisierungen,
- Datenbanken,
- (...).

--

### Digitale Objekte 

**Digitale Objekte aus der Perspektive der Langzeitarchivierung:**
- physisches Objekt (Physikalische Struktur auf Datenträger),  
- logisches Objekt (Binäre Datenstruktur),
- konzeptuelles Objekt (Repräsentation).  

**Digitale Objekte im Datenmanagement:**
- Digitale Objekte beschreiben eine Zusammenstellung von Daten zu einem Gesamtkonstrukt

--
### Beispiel: Tabelle


--

**Informationen als Tabelle**

| Eigenschaft 1 | Eigenschaft  2 |   
|---------------|----------------|
| Datum 1       | Datum 3        |
| Datum 2       | Datum 4        |   

**und in als Text (CSV) dargestellt** 
```csv
Eigenschaft, Datum
Eigenschaft 1, Datum 1
Eigenschaft 1, Datum 2
Eigenschaft 2, Datum 3
Eigenschaft 2, Datum 4
```
Die Informationen können in mehreren Dateien aufgeteilt sein und auf unterschiedliche Weise dargestellt werden.

--

## Metadaten

--

### Metadaten
- Metadaten beschreiben die Forschungsdaten und reichern deren Inhalt um zusätzliche Kontextinformationen an. 
- Informationen können sich auf den Inhalt, das Format oder die Struktur beziehen. Beispiele:
 - Formatangaben zu Bildern
 - Zusatzinformationen zu Stilmerkmale in Texten
 - Technische Informationen zum Dateiformat
 - Vergleichbar zu Metadaten physikalischer Medien und Objekte (etwa Verlagsort, Seitenzahl, Auflage, Erscheinungsjahr, Gewicht einer Brosche)

--

<img src="img/fontane2.png" width="700"  />

---

<!-- .slide: data-background="img/StockSnap_EVVGI0TT1M.jpg" data-background-opacity="0.1"-->
## Forschungsdaten im Forschungsprozess

--
### Beispiel: Forschungsprozess (empirisch, Theoriebildung)
![Prozess](img/forschungsprozess.png)

--
### Beispiel: Lebenszyklus
<img src="img/zyklus.png" width="700"  />

--
### Beispiel: Antragsstellung
<img src="img/dfg-antrag.png" width="500"  />

note: 
- In der Antragsstellung wird diese Sicht wichtig
- Forschungsdatenmangementpläne verlangen nach einer Strategie für die 4 Stationen im Leben
- Forschungsdaten begleiten den Prozess von Anfang bis Ende

---

## Herausforderungen
<!-- .slide: data-background="img/CYM76QR0MT.jpg" data-background-opacity="0.1"-->

--

### Datenformate und Metadaten
* Datenformate beschreiben den "Dialekt" der Daten
 * "Offene Formate" erhöhen Nachhaltigkeit
 * Standards sind wichtig zur Rekonstruktion

* Metadaten beschreiben die Daten

  ... auf technischer, organisatorischer und inhaltlicher Ebene
  ... sind zur Organisation und Kontextualisierung notwendig

--

### Datenformaten
* Datenformate ändern sich über die Zeit
* Datenkonvertierungen führen möglicherweise zu Datenverlusten
* Langzeitstabile Formate sollten bevorzugt werden (Beispiel PDF-A, XML-basierte Standards)
* Idealerweise ist das gewählte Format 
 * Simpel
 * Verbreitet
 * Offen 

--

<img src="img/medien.png" width="800"  />

note:
[google Services](https://gcemetery.co/)

--

### Lebensdauer von Speichermedien
- Die Lebensdauer hängt stark von der Qualität der Medien und der Aufbewahrung ab
- Magnetspeicher (Disketten, Festplatten, Speicherbänder) haben eine zu erwartende Lebensdauer [zwischen 10 und 30 Jahren](http://web.archive.org/web/20040614211040/http://www.oit.umass.edu/publications/at_oit/Archive/fall98/media.html) 
- Optische Medien (etwa CDs, DVDs, ...) liegen je nach Speicherart (Pressung, "Brennen") bei etwa 10 - 80 Jahren 
- Andere Medien (USB Sticks, SSDs, ...) werden vermutlich kurzlebiger sein
- Daten bleiben nur bei Verwendung beständig
- Computer und entsprechende Interfaces entwickeln sich weiter

--

### Software und Vernetzung
* Software überaltert schnell (Nutzergewohnheiten, Weiterentwicklung der Technik, Sicherheit)
* Vernetzte Daten lassen sich nicht mehr einfach abgrenzen
  * Werden Daten oder Services aus mehreren Domainen zusammengezogen, kann dies zu Problemen führen-
  * Lizenzen sollten immer bedacht und geklärt werden
  * Für die Referenzierung von Daten sollten Persistente Identifikatoren verwendet werden (etwa DOI oder handle)

--

### Standards
* Für Datenformate, Metadaten und Schnittstellen existieren diverse Standards
* Standards ermöglichen den Datenaustausch und sichern die Nachhaltigkeit
* Existierende Standards sollten bevorzugt werden, sonst ...

--

![Standards](img/standards.png)



---

## Nachhaltigkeit
<!-- .slide: data-background="img/Acta_Eruditorum_-_I_geroglifici,_1714_–_BEIC_13384397.jpg" data-background-opacity="0.1"-->

--

### Lebensdauer von Datenformaten
- Formate überleben nur, wenn sie genutzt werden
- Vendor-Lock-in sollte vermieden werden
- Offene Formate erlauben eine einfachere Rekonstruktion
- Standards sind wichtig, um die Daten zu rekonstruieren und zuzuordnen
- Konvertierungen zwischen Formaten kann zur Datenverlusten führen und sollten gut bedacht sein 

--

### Lebensdauer digitaler Daten
- Neben Formaten und Speichermedien entscheidet Nachfrage über den Erhalt von Daten (Daten auffindbar machen)
- Je besser die Daten beschrieben sind, desto besser lassen diese sich auffinden (Metadaten pflegen)
- "LOCKSS" - [Lots of Copies Keep Stuff Safe](https://www.lockss.org/) (Sicherungen)
- Klare Lizenzen / Rechtsvereinbarungen erleichtern die Nachnutzung und somit das digitale Vortbestehen (Rechte festlegen)
- Datenmanager und Repositorien unterstützen und geben Empfehlungen (Experten frühzeitig einbeziehen)

--

### Was sollte beachtet werden?

**Frühzeitig planen und Serviceangebote nutzen**
- Bei Projektbeginn Konzept für Datenmanagement erstellen
- Erhebung, Auswertung und Speicherung bedenken
- Anforderungen der Förderer beachten

**Realistische Anforderungen definieren**
- Was soll/muss langfristig erhalten bleiben?
- Welche Rechte sind notwendig? 
- Wie frei können und dürfen die Daten sein?

--

### Datenschutz

**Personenbezogene Daten und Urheberrecht**
- Rechte / Einverständnis klären
- gut dokumentieren
- Auch mit der Infrastruktur rücksprechen

**Digitale Diensten (etwa digitale Edition)** 
- [DSGVO](https://dejure.org/gesetze/DSGVO) kann hier relevant sein
- Abhängigkeiten zu anderen Diensteanbietern beachten 


--

### [DARAIH Repository](https://www.de.dariah.eu)

<img src="img/dariah-rep.png" width="600"  />

--

### [Radar Repository](https://www.radar-service.eu/de/home)

<img src="img/radar-rep.png" width="600"  />

---

<!-- .slide: data-background="img/StockSnap_6RHKEELJ8F.jpg" data-background-opacity="0.05"-->
## Forschungsförderungen

--
### [DFG-Leitlinie: Umgang mit Forschungsdaten](https://www.dfg.de/foerderung/antrag_gutachter_gremien/antragstellende/nachnutzung_forschungsdaten/index.html)

<img src="img/dfg-forschungsdaten.png" width="600"  />

--

<!-- .slide: data-background="img/StockSnap_BEL7YED80C.jpg" data-background-opacity="0.05"-->
### Forschungsdatenmanagement 

**Forschungsdatenmanagement Pläne**
- Skizzieren die Verwendung und Speicherung von digitalen Daten in Projekten
- Werden bereits in einigen Förderlinien vorausgesetzt

**Repositorien**
- Erlauben die nachhaltige Speicherung von Daten
- Können die Verbreitung der eigenen Forschung fördern
- Datenpublikationen finden zunehmend Anerkennung


---

## Danke

---

## Weitere Begriffe

--

- Persistente Identifikatoren (kurz PID, z.B. DOI, Handle, URN): Stabile Referenz für digitale Daten.
- Datenmanagementpläne (DMP): Beschreibung zur nachhaltigen Sicherung von Daten und Infrastrukturen in Drittmittelanträgen.
- Dateninfrastrukturen: (Virtuelle) Einrichtungen (etwa vergleichbar zur Bibliothek), welche Informationen speichern, bereitstellen, aufbereiten und in der Thematik beraten.
- Repository: Ein Verzeichnis (oft auch Speicher) für digitale Forschungsdaten.

--

- Lizenz: Regelt die Nachnutzbarkeit von Daten und klärt die Rechte (etwa Creative-Commons, kurz CC).
- Datenkuration: Die Aufbereitung von Daten aus der Forschung zur Speicherung und Nachnutzung (ähnlich zum Lektorat).
- Datenmapping: Die Verbindung von Daten aus unterschiedlicher Quelle auf einen gemeinsamen Standard.
- Datenrichtlinie: Gibt Standards zum Umgang mit Daten durch Institutionen (Universitäten) und Forschungsförderern an.
- Datenschutz: Dieser spielt bei allen Daten mit Bezug zu personenbezogenen Daten eine wichtige Rolle.

--

- Gute wissenschaftliche Praxis: Die DFG Anforderung, dass "Primärdaten als Grundlagen für Veröffentlichungen auf haltbaren und gesicherten Trägern in der Institution, wo sie entstanden sind, zehn Jahre lang aufbewahrt werden".
- Ingest: Das Einspielen von Daten in ein Repositorium.
- Langzeitarchivierung: Die langfristige und nachhaltige Speicherung von Daten in einem spezifischen und nachhaltigen Archiv. 
- Open Access: Geregelter (idr. freier) Zugang zu Daten und Publikationen.
- ...

-- 
