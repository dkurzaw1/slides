# Public talks made with reaveal.js
This repository contains public presentations made with reveal

## Talks
1. [Einführung in das Thema "Forschungsdaten"](https://dkurzaw1.pages.gwdg.de/slides/Einfuehrung_Forschungsdaten/slides/#/)
2. [Simulation of Research (to be updated)](...)


## Credits
1. [Reveal.js](https://github.com/hakimel/reveal.js)
2. [Template Mathias Göbel](https://mgoebel.pages.gwdg.de/slides/)